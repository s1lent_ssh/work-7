#include <memory>
#include <tins/tins.h>
#include <chrono>

#include "support_stuff.hpp"
#include "hurst_calculator.hpp"

using std::cout, std::cerr, std::endl;
using namespace std::chrono;
using namespace Tins;

int main(int argc, const char* argv[]) {
    //Parameters stuff
    std::vector<std::string> params(argv + 1, argv + argc);
    auto [interface, count, filter] = support_stuff::params_parser(params);

    //Sniffer init 
    std::unique_ptr<Sniffer> sniffer;
    try {
        SnifferConfiguration config;
        config.set_immediate_mode(true);
        config.set_filter(filter);
        sniffer = std::make_unique<Sniffer>(interface, config);
    } catch (const invalid_pcap_filter&)  {
        cerr << "Error: Input valid filter" << endl;
        return support_stuff::BAD_EXIT;
    } catch (const pcap_error&) {
        cerr << "Error: No such interface" << endl;
        return support_stuff::BAD_EXIT;
    }

    //Data vector init
    std::vector<double> time_data;
    time_data.reserve(count);

    //Sniff loop
    cout << "Start sniffing." << endl << endl;

    auto last_time = system_clock::now();

    sniffer->sniff_loop([&time_data, &last_time, count](const Packet& packet) {
        auto new_time = system_clock::now();
        duration<double, std::milli> delta = new_time - last_time;
        time_data.push_back(delta.count());

        support_stuff::draw_progress_bar(static_cast<float>(time_data.size()) / count);
        last_time = new_time;

        if(time_data.size() == count + 1)
            return false;
        else
            return true;
    });
    time_data.at(0) = 0;

    cout << "Sniffing done." << endl;
    cout << "[Hurst] " << HurstCalculator<double>()(time_data) << endl;
}
