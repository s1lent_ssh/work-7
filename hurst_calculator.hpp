#include <cmath>
#include <numeric>

template<typename T>
class HurstCalculator {
    static constexpr T _CONSTANT = std::atan(1.) * 2;

public:
    T operator()(std::vector<T> data) const noexcept {
        const auto PACKETS_COUNT = data.size();

        const T average_value = std::accumulate(data.begin(), data.end(), .0) / PACKETS_COUNT;

        const T mean_deviation = [&]() {
            T result {.0};
            for(const auto value : data)
                result += std::pow(value - average_value, 2);
            return std::sqrt(result / PACKETS_COUNT);
        }();
    
        std::vector<T> accumulated_deviation;
        accumulated_deviation.reserve(PACKETS_COUNT);
        for(const auto value : data) 
            accumulated_deviation.push_back(value - mean_deviation);

        const auto [min, max] = std::minmax_element(accumulated_deviation.begin(), accumulated_deviation.end());
        const T swing_range = max - min;

        const T hurst_coefficient = std::log(swing_range / mean_deviation) / std::log(_CONSTANT * PACKETS_COUNT);

        return hurst_coefficient;
    }
};
