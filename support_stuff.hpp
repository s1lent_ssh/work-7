#include <iostream>
#include <tuple>
#include <vector>
#include <cmath>

namespace support_stuff {

using std::cout, std::cerr, std::endl;

const int BAD_EXIT {0};

void draw_progress_bar(float progress) {
    const int   BAR_WIDTH {73};
    const char  BAR_SYMBOL {'='};
    const char  BAR_EMPTY {' '};

    int position = BAR_WIDTH * progress;
    cout << "\b\r[";
    for(int i = 0; i < position; i++) 
        cout << BAR_SYMBOL;
    for(int i = position; i < BAR_WIDTH; i++)
        cout << BAR_EMPTY;
    cout << "] " << std::floor(progress * 100) << "%" << endl;
}

auto params_parser(std::vector<std::string> params) {
    std::string interface {"enp0s3"};
    int         packets_count {3000};
    std::string filter {""};

    if(params.empty()) {
        cout << "Using default parametres" << endl;
        return std::make_tuple(interface, packets_count, filter);
    }
    interface = params.at(0);

    if(params.size() >= 2) {
        try {
            packets_count = std::stoi(params.at(1));
            if(packets_count < 3000) {
                packets_count = 3000;
                cerr << "Too little packets count. Using default value " << packets_count << endl;
            }
        } catch (std::invalid_argument&) {
            cerr << "Invalid packets count. Using default value" << endl;
        }
    }

    if(params.size() >= 3)
        filter = params.at(2);

    return std::make_tuple(interface, packets_count, filter);
}

}